import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashSet;
import java.util.Set;

public class Server extends Thread {
    private static final int PORT = 5021;
    private final Set<BufferedWriter> clients = new HashSet<>();
    private char[] buffer = new char[1024];
    private String message;

    public static void main(String[] args) {
        System.out.println(
                "-------------------------------------------------\n" +
                        "Internet Technology & Web Engineering - Exercise1\n" +
                        " Marius Hamacher (300 569 3)\n" +
                        " Marcel Brauer (228 260 5)\n" +
                        " Lucas Hillebrand (300 638 6)\n" +
                        "-------------------------------------------------\n\n");
        new Server().start();
    }

    @Override
    public void run() {
        try (ServerSocket serverSocket = new ServerSocket(PORT)) {
            System.out.println("SERVER: Listening on port " + PORT + "...");
            while (true) {
                new MessageHandlingThread(serverSocket.accept()).start();
                System.out.println("SERVER: Client connected.");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private synchronized void broadcastMessage(String message) {
        System.out.println("SERVER: Broadcasting message to all clients: '" + message + "'");
        for (BufferedWriter toClient : clients) {
            try {
                toClient.write(message);
                toClient.flush();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private class MessageHandlingThread extends Thread {
        Socket clientSocket;
        BufferedWriter toClient;

        public MessageHandlingThread(Socket clientSocket) {
            this.clientSocket = clientSocket;
        }

        @Override
        public void run() {
            try (InputStreamReader fromClient = new InputStreamReader(clientSocket.getInputStream());
                 BufferedWriter toClient = new BufferedWriter(new OutputStreamWriter(clientSocket.getOutputStream()))
            ) {
                this.toClient = toClient;

                synchronized (clients) {
                    clients.add(toClient);
                }

                while (!clientSocket.isClosed()) {
                    if (fromClient.read(buffer) > 0) {
                        message = new String(buffer);
                        System.out.println("Server received message: " + message);
                        broadcastMessage(message);
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                System.out.println("SERVER: Client disconnected.");
                synchronized (clients) {
                    clients.remove(this.toClient);
                }
            }
        }
    }
}
