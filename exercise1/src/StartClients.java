public class StartClients {
    public static void main(String[] args) throws Exception {
        System.out.println("Make sure to start the server first.");
        Thread client1 = new Client(1, "Client 1: spam");
        Thread client2 = new Client(2, "Client 2: first!");

        client1.start();
        Thread.sleep(3000);
        client2.start();
    }
}
