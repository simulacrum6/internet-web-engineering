import java.io.*;
import java.net.Socket;
import java.util.Random;

public class Client extends Thread {
    private int id;
    private String message;
    private char[] inputBuffer = new char[1024];

    public Client(int id, String message) {
        this.id = id;
        this.message = message;
    }

    @Override
    public void run() {
        System.out.println("CLIENT" + id + " started. \n message: " + message);

        try (Socket serverSocket = new Socket("localhost", 5021);
             BufferedWriter toServer = new BufferedWriter(new OutputStreamWriter(serverSocket.getOutputStream()))
        ) {
            Thread receive = new ReceiveThread(serverSocket);
            receive.start();

            while (!serverSocket.isClosed()) {
                toServer.write(message);
                toServer.flush();
                System.out.println("CLIENT" + id + " sent message: " + message);
                Thread.sleep(new Random().nextInt(4000));
            }
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        } finally {
            System.out.println("CLIENT" + id + " stopped.");
        }
    }

    private class ReceiveThread extends Thread {
        private Socket serverSocket;

        public ReceiveThread(Socket serverSocket) {
            this.serverSocket = serverSocket;
        }

        @Override
        public void run() {
            try (BufferedReader fromServer = new BufferedReader(new InputStreamReader(serverSocket.getInputStream()))) {
                while (!serverSocket.isClosed()) {
                    if (fromServer.read(inputBuffer) > 0) {
                        System.out.println("CLIENT" + id + " received message: '" + new String(inputBuffer) + "'");
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
