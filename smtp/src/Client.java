import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.Socket;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;

public class Client {
    // connection info
    private static final String MAIL_SERVER = "debby.vs.uni-due.de";
    private static final int PORT = 25;

    // SMTP messages
    private static final String GREETING = "HELO %s\r\n";
    private static final String MAIL_FROM = "MAIL FROM: <%s>\r\n";
    private static final String RCTP_TO = "RCPT TO: <%s>\r\n";
    private static final String DATA = "DATA\r\n";

    private List<String> messages;
    private char[] buffer = new char[1024];
    private String response;

    public static void main(String[] args) throws Exception {
        System.out.println(
                "-------------------------------------------------\n" +
                        "Internet Technology & Web Engineering - Exercise5\n" +
                        " Marius Hamacher (300 569 3)\n" +
                        " Marcel Brauer (228 260 5)\n" +
                        " Lucas Hillebrand (300 638 6)\n" +
                        "-------------------------------------------------\n\n");

        String[] attachments = args.length > 0 ? args : new String[0];
        String text = "Некоторые иностранцы думают, что в России медведи ходят по улицам." +
                " Конечно, это неправда! Медведи живут в лесу и не любят людей.\n" +
                "Встреча с медведем может быть очень опасна. Русские люди любят ходить в лес и собирать грибы и ягоды." +
                " Они делают это с осторожностью, так как медведи тоже очень любят ягоды и могут напасть на человека. " +
                "Медведь ест всё: ягоды, рыбу, мясо и даже насекомых. Особенно он любит мёд.";

        Mail mail = new Mail(
                "mhama@test.spammalicious",
                "spam@debby.vs.uni-due.de",
                "hello",
                text,
                attachments);

        Client client = new Client();
        client.send(mail);
    }

    public boolean send(Mail mail) throws IOException {
        prepareSMTPMessages(mail);

        try (Socket socket = new Socket(MAIL_SERVER, PORT);
             InputStreamReader fromServer = new InputStreamReader(socket.getInputStream());
             OutputStream toServer = socket.getOutputStream()
        ) {
            for (String message : messages) {
                fromServer.read(buffer);
                response = new String(buffer);

                if (responseIsFine(response)) {
                    toServer.write(message.getBytes(Charset.forName("US-ASCII")));
                } else {
                    System.err.println(response);
                    return false;
                }
            }

            // print id
            fromServer.read(buffer);
            response = new String(buffer);

            if (responseIsFine(response)) {
                System.out.println("Mail sent.\n" + response.split("\n")[0]);
                return true;
            }

            System.err.println("Mail could not be sent!\n" + response);
            return false;
        }
    }

    private void prepareSMTPMessages(Mail mail) {
        messages = new ArrayList<>();
        messages.add(String.format(GREETING, mail.getSenderName()));
        messages.add(String.format(MAIL_FROM, mail.getSenderMail()));
        messages.add(String.format(RCTP_TO, mail.getRecipientMail()));
        messages.add(DATA);
        messages.add(mail.getMailContent());
    }

    private boolean responseIsFine(String response) {
        int statusCode = Integer.parseInt(response.split(" ")[0]);
        return statusCode >= 200 && statusCode < 500;
    }
}
