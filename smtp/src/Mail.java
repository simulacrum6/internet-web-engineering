import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;
import java.util.UUID;

public class Mail {
    private static final String BOUNDARY = "--ANsvqthY3arCd5QJ3";
    private static final String TERMINATOR = ".\r\n";

    private String from;
    private String to;
    private String subject;
    private String messageId;
    private List<MailComponent> components;

    public Mail(String from, String to, String subject, String text, String[] attachmentsFiles) {
        this.from = from;
        this.to = to;
        this.subject = subject;
        this.messageId = UUID.randomUUID().toString();
        this.components = new ArrayList<>();

        this.components.add(MailComponent.fromText(text));
        for (String filePath : attachmentsFiles) {
            this.components.add(MailComponent.fromFile(filePath));
        }
    }

    @Override
    public String toString() {
        String mail = "From: " + from + "\n" +
                "To: " + to + "\n";

        return mail + getMailContent();
    }

    public String getSenderName() {
        return from.split("@")[0];
    }

    public String getSenderMail() {
        return from;
    }

    public String getRecipientMail() {
        return to;
    }

    public String getMailContent() {
        String mail =
                "Subject: " + subject + "\n" +
                "Mime-Version: 1.0\n" +
                "Content-Type: multipart/mixed; boundary=\"" + BOUNDARY.replace("--", "") + "\"\n" +
                "Content-Disposition: inline\n\n";

        for (MailComponent component : components) {
            mail += BOUNDARY + "\n" + component.toString();
        }

        return mail + TERMINATOR;
    }
}

class MailComponent {
    private static Base64.Encoder encoder = Base64.getMimeEncoder();

    private String contentType;
    private String contentTransferEncoding;
    private String data;
    private String contentDisposition;

    private MailComponent() {
    }

    public static MailComponent fromText(String text) {
        MailComponent component = new MailComponent();
        component.contentType = "plain/text; charset=\"UTF-8\"";
        component.contentDisposition = "inline";
        component.contentTransferEncoding = "base64";
        component.data = encodeText(text);
        return component;
    }

    public static MailComponent fromFile(String filePath) {
        File file = new File(filePath);
        MailComponent component = new MailComponent();
        component.contentType = resolveContentType(file);
        component.contentDisposition = "attachment; filename=\"" + file.getName() + "\"";
        component.contentTransferEncoding = "base64";
        component.data = encodeFile(file);
        return component;
    }

    private static String resolveContentType(File file) {
        // TODO: Add more filetypes?
        if (file.getName().endsWith("jpeg"))
            return "image/jpeg";
        return "application/octet-stream; name=\"" + file.getName() + "\"";
    }

    private static String encodeFile(File file) {
        String encoded = "";
        Path path = Paths.get(file.getAbsolutePath());
        try {
            byte[] bytes = Files.readAllBytes(path);
            encoded = encoder.encodeToString(bytes);
        } catch (IOException e) {
            System.err.println("Could not encode attachment " + file.getAbsolutePath());
        } finally {
            return encoded;
        }
    }

    private static String encodeText(String text) {
        try {
            return encoder.encodeToString(text.getBytes("UTF-8"));
        } catch (UnsupportedEncodingException e) {
            System.err.println("Could not encode the following text (encoding not supported):\n" + text);
            return "";
        }
    }

    @Override
    public String toString() {
        String result =
                "Content-Type: " + this.contentType + "\n" +
                "Content-Disposition: " + this.contentDisposition + "\n" +
                "Content-Transfer-Encoding: " + this.contentTransferEncoding + "\n\n" +
                this.data + "\n\n";
        return result;
    }
}

